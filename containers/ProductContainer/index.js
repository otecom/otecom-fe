import { useEffect } from 'react';
import { useState } from 'react';
import Breadcrumb from '~/components/base/Breadcrumb';
import Button from '~/components/base/Button';
import Container from '~/components/base/Container';
import Section from '~/components/base/Section';
import TitleSection from '~/components/base/TitleSection';
import DetailProduct from '~/components/product/DetailProduct';
import LikeBrand from '~/components/product/LikeBrand';
import { getProducts, useGetProducts } from '~/hooks/api/useProduct';

import styles from './styles.module.scss';

export default function ProductContainer({ data }) {
  const [readMoreDes, setReadMoreDes] = useState(false);
  const [likeBrand, setLikeBrand] = useState([]);
  useEffect(() => {
    if (data) {
      getProducts({
        limit: 100,
        category: data?.category?.slug
      }, {
        pageParam: 1,
      }).then((result) => {
        setLikeBrand(result?.data)
      }).catch((err) => {
        console.log({ err })
      });
    }
  }, [data])
  return (
    <>
      {
        data && <Container>
          <Breadcrumb
            data={[
              {
                text: 'Trang chủ',
                to: '/',
              },
              {
                text: data?.category?.name,
                to: '/category/' + data?.category?.slug,
              },
              {
                text: data?.name,
                to: '#',
              },
            ]}
          />
          <Section>
            <DetailProduct data={data} />
          </Section>
          <Section>
            <LikeBrand data={likeBrand} />
          </Section>
          <Section>
            <TitleSection text={'Thông tin sản phẩm'}></TitleSection>
            <hr className={styles['hr']} />
            <div
              id="x2zk"
              className={`${styles['description-detail']} ${readMoreDes ? '' : styles['clampText']}`}
              dangerouslySetInnerHTML={{
                __html: data?.description,
              }}
            >
              { }
            </div>
            <Button center={true} style={{
              marginTop: '16px'
            }} onClick={() => {
              setReadMoreDes(!readMoreDes)
            }}>{
                readMoreDes ? <a href="#x2zk">Rút Gọn</a> : <span>Xem Thêm</span>
              }</Button>
          </Section>
        </Container>
      }
    </>
  );
}
