import Container from '~/components/base/Container';
import Section from '~/components/base/Section';
import BannerSlider from '~/components/home/BannerSlider';
import ListCategories from '~/components/home/ListCateogories';
import TrendingCategories from '~/components/home/TrendingCategories';

function HomeContainer() {
  return (
    <>
      <Container>
        <BannerSlider />
      </Container>

      <Section>
        <ListCategories />
      </Section>

      <Section>
        <TrendingCategories />
      </Section>
    </>
  );
}

export default HomeContainer;
