import FontIcon from '../FontIcon';
import styles from './styles.module.scss';
import baseCss from '~/public/styles/base.module.scss';

export default function TitleSection({ children, text }) {
  return (
    <h3 className={styles['title']}>
      <FontIcon
        size="28px"
        icon={9810}
        options={{
          fill: 1,
        }}
        color={baseCss.colorPrimary}
      >
        {children}
      </FontIcon>
      {text}
    </h3>
  );
}
