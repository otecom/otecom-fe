import Link from 'next/link';
import styles from './styles.module.scss';

export default function Breadcrumb({ data = {} }) {
  return (
    <div className={styles['breadcrumb']}>
      {data?.map((item, index) => {
        return (
          <>
            <Link href={item?.to || '/'}>
              <a className={styles['item']} title={item?.text}>
                {item?.text}
              </a>
            </Link>
            {index < data?.length - 1 && <p className={styles['symbol']}>{'>'}</p>}
          </>
        );
      })}
    </div>
  );
}
