import styles from './styles.module.scss';

export default function SlideItem({ children, style }) {
  return <div className={styles['slide-item']} style={style}>{children}</div>;
}
