import Container from '../Container';
import styles from './styles.module.scss';
export default function Section({ children }) {
  return (
    <Container>
      <div className={styles['section']}>{children}</div>
    </Container>
  );
}
