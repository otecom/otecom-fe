import styles from './styles.module.scss';
export default function BackdropModal() {
    return <div className={styles['backdrop']}></div>
}