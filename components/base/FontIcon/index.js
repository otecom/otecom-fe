// import 'font-awesome/css/font-awesome.min.css';

export default function FontIcon({ children, color, size = '32px', options = {}, onClick = () => {} }) {
  const { fill = 0, wght = 400, grad = 0, opsz = 48 } = options;
  //how to write codepoint? &#x{code-point};
  return (
    <>
      <span
        className="material-symbols-outlined"
        style={{
          color: color,
          fontSize: size,
          fontVariationSettings: `'FILL' ${fill}, 'wght' ${wght}, 'GRAD' ${grad}, 'opsz' ${opsz}`,
          userSelect: 'none',
        }}
        onClick={onClick}
      >
        {children}
      </span>
    </>
  );
}
