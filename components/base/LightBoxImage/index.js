import { useImperativeHandle, useState } from 'react';
import { forwardRef } from 'react';
import FontIcon from '../FontIcon';
import styles from './styles.module.scss';

function LightBoxImage({ images = [] }, ref) {
  const [indexNow, setIndexNow] = useState(0);
  const [showLightBox, setShowLightBox] = useState(false);
  const [zoomRatio, setZoomRatio] = useState(1);
  useImperativeHandle(ref, () => {
    return {
      show(number) {
        setShowLightBox(true);
        setIndexNow(number);
      },
    };
  });
  function closePopup(e) {
    if (e.target === e.currentTarget) {
      setShowLightBox(false);
    }
  }
  function zoomImage(e) {
    if (zoomRatio <= 1.5) {
      setZoomRatio(1.8);
    } else {
      setZoomRatio(1);
    }
  }
  return (
    <div className={`${styles['light-box-image']} ${showLightBox ? styles['show'] : ''}`} onClick={closePopup}>
      <span
        className={`${styles['arrow']} ${styles['prev']} ${indexNow == 0 ? styles['disabled'] : ''}`}
        onClick={() => {
          setIndexNow((prev) => prev - 1);
        }}
      >
        <FontIcon>&#xe5c4;</FontIcon>
      </span>
      <img
        style={{
          transform: `scale(${zoomRatio})`,
          cursor: zoomRatio >= 1.5 ? 'zoom-out' : 'zoom-in',
        }}
        className={styles['img-show']}
        src={images[indexNow]}
        onClick={zoomImage}
      />
      <span
        onClick={() => {
          setIndexNow((prev) => prev + 1);
        }}
        className={`${styles['arrow']} ${styles['next']} ${indexNow >= images.length - 1 ? styles['disabled'] : ''}`}
      >
        <FontIcon>&#xe5c8;</FontIcon>
      </span>
    </div>
  );
}

export default forwardRef(LightBoxImage);
