import SlideToView from '~/components/base/SlideToView';
import SlideItem from '~/components/base/SlideToView/SlideItem';
import Space from '~/components/base/Space';
import TitleSection from '~/components/base/TitleSection';
import { useGetCategories } from '~/hooks/api/useCategories';

import ItemCategories from './ItemCategories';

import styles from './styles.module.scss';

export default function ListCategories() {
  const { data: categories } = useGetCategories();

  return (
    <>
      <TitleSection text={'Danh mục sản phẩm'}>&#xE1A0;</TitleSection>

      <Space height={24} />
      <SlideToView classFrame={styles['list']} scrollBy={150}>
        {categories?.data?.map((item, index) => (
          <SlideItem>
            <ItemCategories key={index} data={item} />
          </SlideItem>
        ))}
      </SlideToView>
    </>
  );
}
