import Link from 'next/link';
import ItemCategoriesImg from 'public/imgs/notData/bookDemoCategory.png';

import styles from './styles.module.scss';

export default function ItemCategories({ data }) {
  return (
    <Link href={'/category/' + data?.slug}>
      <a className={styles['item']}>
        <img className={styles['img']} src={data?.image || ItemCategoriesImg.src} />
        <p className={styles['name']}>{data?.name}</p>
      </a>
    </Link>
  );
}
