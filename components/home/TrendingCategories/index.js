import styles from './styles.module.scss';
import TitleSection from '~/components/base/TitleSection';
import { useGetCategories } from '~/hooks/api/useCategories';
import ListKeywords from '~/components/common/ListKeywords';
import Space from '~/components/base/Space';
import { useGetProductsInfinite } from '~/hooks/api/useProduct';
import ItemShop from '~/components/common/ItemShop';
import { Fragment } from 'react';
import Button from '~/components/base/Button';
import { useState } from 'react';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import LoadingIcon from '~/public/imgs/icons/javascript/loading-icon';

export default function TrendingCategories() {
  const router = useRouter();
  const { data: categories } = useGetCategories({
    sorting: 'ASC',
    limit: 4,
  });
  const [categoryActive, setCategoryActive] = useState('');
  const { data, error, fetchNextPage, hasNextPage, isFetching, isFetchingNextPage, status } = useGetProductsInfinite({
    category: categoryActive?.slug,
    limit: 10,
  });
  useEffect(() => {
    let _k_ = categories?.data[0];
    setCategoryActive(_k_);
  }, [categories]);
  return (
    <div>
      <TitleSection text={'Xu hướng danh mục'}>&#xE8E5;</TitleSection>
      <Space height={20} />
      <ListKeywords
        data={categories?.data?.map((item, index) => {
          return {
            title: item?.name,
            slug: item?.slug,
            index: index,
          };
        })}
        onChange={(item) => {
          setCategoryActive(categories?.data[item?.index]);
        }}
      />
      <Space height={14} />

      {data ? (
        <div className={styles['list-item']}>
          {data?.pages?.map((group, i) => {
            return (
              <Fragment key={i}>
                {group?.data?.map((item, j) => {
                  // console.log(item);
                  return <ItemShop key={i.toString() + '-' + j} data={item} />;
                })}
              </Fragment>
            );
          })}
        </div>
      ) : (
        <div
          style={{
            width: '100%',
            height: '100%',
            minHeight: '250px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <LoadingIcon />
        </div>
      )}

      {hasNextPage && (
        <>
          <Space height={40} />
          <Button
            center={true}
            onClick={() => {
              router.push(`/category/${categoryActive?.slug}`);
            }}
          >
            Xem Thêm
          </Button>
        </>
      )}
    </div>
  );
}
