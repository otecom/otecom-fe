import { useGetBanner } from '~/hooks/api/useBanner';

import styles from './styles.module.scss';

import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { useRef } from 'react';
import FontIcon from '~/components/base/FontIcon';

function Banner({ data }) {
  return (
    <div className={styles['banner']}>
      <img src={data?.image_url} />
      <div className={styles['description']}>
        <h6 className={styles['title-banner']}>{data?.title}</h6>
        <p className={styles['des-banner']}>{data?.description}</p>
      </div>
    </div>
  );
}

function ArrowSlide({ children, style, onClick }) {
  return (
    <div className={styles['arrow']} style={{ ...style }} onClick={onClick}>
      <FontIcon color={'#333'} size="20px">
        {children}
      </FontIcon>
    </div>
  );
}

export default function BannerSlider() {
  const { data } = useGetBanner();
  const leftRef = useRef();
  const rightRef = useRef();
  // console.log(leftRef);
  const settings = {
    className: styles['banner'],
    centerPadding: '0px',
    centerMode: true,
    dotsClass: `slick-dots slick-thumb`,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <p ref={rightRef}></p>,
    prevArrow: <p ref={leftRef}></p>,
    // responsive: [
    //   {
    //     breakpoint: PC_WIDTH,
    //     settings: {
    //       centerPadding: '20px',
    //     },
    //   },
    //   {
    //     breakpoint: MOBILE_WIDTH,
    //     settings: {
    //       centerPadding: '0px',
    //     },
    //   },
    // ],
  };
  return (
    <div className={styles['slider']}>
      <Slider {...settings}>
        {data?.data?.map((item, index) => {
          return <Banner key={index} data={item} />;
        })}
      </Slider>

      {data?.data?.length > 1 && (
        <>
          {' '}
          <ArrowSlide
            onClick={() => {
              leftRef.current.click();
            }}
            style={{
              left: '-20px',
            }}
          >
            &#xe5cb;
          </ArrowSlide>
          <ArrowSlide
            onClick={() => {
              rightRef.current.click();
            }}
            style={{
              right: '-20px',
            }}
          >
            &#xe5cc;
          </ArrowSlide>
        </>
      )}
    </div>
  );
}
