import SlideToView from "~/components/base/SlideToView";
import SlideItem from "~/components/base/SlideToView/SlideItem";
import Space from "~/components/base/Space";
import TitleSection from "~/components/base/TitleSection";
import ItemShop from "~/components/common/ItemShop";

import styles from './styles.module.scss';

export default function LikeBrand({ data }) {
    return (
        <div>
            <TitleSection text={'Sản phẩm cùng thương hiệu'}>&#xe907;</TitleSection>

            <Space height={24} />
            <SlideToView classFrame={styles['listed']} scrollBy={150}>
                {data?.map((item, index) => (
                    <SlideItem style={{ paddingTop: '16px' }}>
                        <ItemShop key={index} data={item} width='200px' />
                    </SlideItem>
                ))}
            </SlideToView>
        </div>
    )
}