import styles from './styles.module.scss';
import { Lightbox } from 'react-modal-image';
import LeftCol from './LeftCol';
import RightCol from './RightCol';

export default function DetailProduct({ data }) {
  return (
    <div className={styles['detail-product']}>
      <LeftCol images={data?.images} />
      <RightCol data={data} />
    </div>
  );
}
