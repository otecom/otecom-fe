import Button from '~/components/base/Button';
import FontIcon from '~/components/base/FontIcon';
import ImageSmall from './ImageSmall';
import styles from './styles.module.scss';

import baseCss from '~/public/styles/base.module.scss';
import { useEffect, useRef } from 'react';
import LightBoxImage from '~/components/base/LightBoxImage';

export default function LeftCol({ images }) {
  const listImage = images?.length > 5 ? [...images].splice(0, 4) : images;
  const lightBoxRef = useRef();

  return (
    <div>
      <LightBoxImage ref={lightBoxRef} images={images} />
      <div className={styles['images']}>
        <div className={styles['list-image']}>
          {listImage?.map((it, id) => {
            return (
              <ImageSmall
                key={id}
                image={it}
                onClick={() => {
                  lightBoxRef.current.show(id);
                }}
              />
            );
          })}
          {images?.length > 5 && (
            <ImageSmall
              number={images?.length - 4}
              onClick={() => {
                lightBoxRef.current.show(4);
              }}
            />
          )}
        </div>

        <div
          className={styles['main-image']}
          onClick={() => {
            lightBoxRef.current.show(0);
          }}
        >
          <img src={images[0]} />
        </div>
      </div>
      <div className={styles['button']}>
        <Button
          style={{
            width: '100%',
          }}
          outline={true}
        >
          <FontIcon
            color={baseCss.colorPrimary}
            size="24px"
            options={{
              wght: 700,
            }}
          >
            &#xe8cc;
          </FontIcon>
          Thêm vào giỏ hàng
        </Button>
        <Button
          style={{
            width: '100%',
          }}
        >
          Mua ngay
        </Button>
      </div>
    </div>
  );
}
