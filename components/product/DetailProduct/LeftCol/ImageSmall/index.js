import styles from './styles.module.scss';
export default function ImageSmall({ image, number = 0, onClick = () => {} }) {
  return (
    <div className={styles['image']} onClick={onClick}>
      {number === 0 ? (
        <img src={image} />
      ) : (
        <div className={styles['other-img']}>
          <span>+{number}</span>
        </div>
      )}
    </div>
  );
}
