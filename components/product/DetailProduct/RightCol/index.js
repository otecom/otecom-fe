import { useState, useLayoutEffect } from 'react';
import { toast } from 'react-toastify';
import { handlePrice } from '~/utils/strings';
import styles from './styles.module.scss';

export default function RightCol({ data }) {
  const [quantity, setQuantity] = useState(0);

  useLayoutEffect(() => {
    if (!quantity || +quantity < 0) {
      setQuantity(0);
    } else if (quantity > data?.quantity_remain) {
      toast.error('Số lượng vượt quá giới hạn!');
      setQuantity(data?.quantity_remain);
    }
  }, [quantity]);

  return (
    <div className={styles['right-col']}>
      <h3>{data?.name}</h3>

      <div className={styles['price-frame']}>
        <p className={styles['real-price']}>{handlePrice(data?.selling_price)} đ</p>
        {data?.selling_price !== data?.original_price && (
          <>
            <p className={styles['ori-price']}>{handlePrice(data?.original_price)} đ</p>
            <span className={styles['decrease-percent']}>
              -{Math.round(((data?.original_price - data?.selling_price) / data?.original_price) * 100)}%
            </span>
          </>
        )}
      </div>
      <div className={styles['number-order']}>
        <span>Số lượng</span>
        <div className={styles['input-frame']}>
          <button
            className={styles['navigate-number']}
            onClick={() => {
              setQuantity((prev) => +prev - 1);
            }}
          >
            -
          </button>
          <input
            type="number"
            className={styles['number-input']}
            value={quantity}
            onChange={(e) => {
              setQuantity(e.target.value);
            }}
          />
          <button
            className={styles['navigate-number']}
            onClick={() => {
              setQuantity((prev) => +prev + 1);
            }}
          >
            +
          </button>
        </div>
      </div>
      <div
        className={`${styles['description']}`}
        dangerouslySetInnerHTML={{
          __html: data?.description,
        }}
      >
        {}
      </div>
    </div>
  );
}
