import Link from 'next/link';
import { handlePrice } from '~/utils/strings';
import styles from './styles.module.scss';

export default function ItemShop({ data, width, height, style }) {
  let priceDiscount = data?.original_price - data?.selling_price;
  let percentDiscount = Math.floor((priceDiscount / data?.original_price) * 100);
  let percentQuantitySold = (data?.quantity_sold / +data?.quantity) * 100;
  return (
    <Link href={`/product/${data?.slug}`}>
      <a className={styles['item-shop']} style={{
        width: width,
        height: height,
        ...style
      }}>
        <div className={styles['img']}>
          <img src={data?.images[0]} />
        </div>
        <p className={styles['name']}>{data?.name}</p>
        <p className={styles['sell-price']}>{handlePrice(data?.selling_price)}đ</p>
        <p
          className={styles['original-price']}
          style={{
            opacity: data?.selling_price < data?.original_price ? 1 : 0,
          }}
        >
          {handlePrice(data?.original_price)}đ
        </p>
        {priceDiscount > 0 && <div className={styles['percent-discount']}>{percentDiscount}%</div>}
        <div className={styles['quantity-sold']}>
          <div
            className={styles['sold-percent']}
            style={{
              width: percentQuantitySold + '%',
            }}
          ></div>
          <p>Đã bán {data?.quantity_sold}</p>
        </div>
      </a>
    </Link>
  );
}
