import IconNav from '../IconNav';
import styles from './styles.module.scss';

export default function ListNav({ device = 'pc' }) {
  return (
    <div className={`${styles['list']} ${styles[device]}`}>
      <IconNav label={'Giỏ Hàng'}>&#xe8cc;</IconNav>
      <IconNav label={'Tài Khoản'}>&#xe7fd;</IconNav>
      <IconNav label={'Cài Đặt'}>&#xe8b8;</IconNav>
    </div>
  );
}
