import { useRef } from 'react';
import { useState } from 'react';
import { addClass, removeClass } from '~/utils/HandleClass';
import styles from './styles.module.scss';

export default function PopupNav({ isOpen }) {
  const popupNavRef = useRef();
  console.log(isOpen, 'a');

  useState(() => {
    if (isOpen) {
      addClass(popupNavRef.current, styles['active']);
    } else {
      removeClass(popupNavRef.current, styles['active']);
    }
  }, [isOpen]);
  return <div ref={popupNavRef} className={styles['popup']}></div>;
}
