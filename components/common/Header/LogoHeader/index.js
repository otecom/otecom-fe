import Link from 'next/link';
import useResize from '~/hooks/useResize';

export default function LogoHeader({ }) {
  const device = useResize();
  return (
    <div>
      <Link href="/">
        <a>
          <img src={device === 'mobile' ? '/logo/logo-icon.svg' : '/logo/logo.svg'} style={{ height: '65px' }} />
        </a>
      </Link>
    </div>
  );
}
