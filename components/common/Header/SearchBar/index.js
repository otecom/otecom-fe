import { useRouter } from 'next/router';
import { useRef } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { toast } from 'react-toastify';
import FontIcon from '~/components/base/FontIcon';
import Input from '~/components/base/Input';
import { useGlobal } from '~/context/GlobalContext';
import { getLocalStorage, setLocalStorage } from '~/utils/localStorage';
import HistorySearch from './HistorySearch';
import styles from './styles.module.scss';

export default function SearchBar() {
  const [value, setValue] = useState('');
  const [historySearch, setHistorySearch] = useState([]);
  const searchBarRef = useRef();
  const router = useRouter();
  useEffect(() => {
    let tmp = getLocalStorage('history-search', []);
    // console.log(tmp, typeof tmp);
    tmp = tmp.length > 0 ? tmp.split(',') : [];
    setHistorySearch(tmp);
    function x(e) {
      if (searchBarRef && searchBarRef.current) {
        const ref = searchBarRef.current;
        if (!ref.contains(e.target)) {
          setIsShowBackdrop(false);
        }
      }
    }
    window.addEventListener('click', x);
    return () => {
      window.removeEventListener('click', x);
    };
  }, []);
  function search() {
    if (value !== '') {
      router.push({
        pathname: '/',
        query: {
          s: value,
        },
      });
      let tmp = historySearch;
      if (tmp.length > 9) {
        tmp.shift();
      }
      tmp.push(value);

      setHistorySearch(tmp);
      setLocalStorage('history-search', tmp);
      setValue('');
    } else {
      toast.error('Không được để trống giá trị!');
    }
  }
  const { isShowBackdrop, setIsShowBackdrop } = useGlobal('backdropModal');
  return (
    <>
      <div className={styles['search-bar']} style={{ flex: '1' }} ref={searchBarRef}>
        <Input
          type="text"
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
          }}
          placeholder={'Tìm kiếm sách mong muốn...'}
          onKeyPressEnter={() => {
            search();
          }}
          onFocus={() => {
            setIsShowBackdrop(true);
          }}
          onClick={() => {
            setIsShowBackdrop(true);
          }}
        />
        <div
          className={styles['btn-search']}
          onClick={() => {
            search();
          }}
        >
          <FontIcon icon={'search'} color="white" size="24px">
            &#xe8b6;
          </FontIcon>
        </div>
        {isShowBackdrop && (
          <div className={styles['option']} onClick={() => {}}>
            <HistorySearch listKeyword={historySearch} setListKeyword={setHistorySearch} />
          </div>
        )}
      </div>
      <div className={styles['backdrop']}></div>
    </>
  );
}
