import { toast } from 'react-toastify';
import FontIcon from '~/components/base/FontIcon';
import { clearLocalStorage, setLocalStorage } from '~/utils/localStorage';
import KeywordSearch from './KeywordSearch';
import styles from './styles.module.scss';

export default function HistorySearch({ listKeyword = [], setListKeyword = () => {} }) {
  return (
    <div className={styles['history-search']}>
      <div className={styles['head']}>
        <FontIcon color="black" size="24px">
          &#xe889;
        </FontIcon>
        Lịch sử tìm kiếm
        <div
          className={styles['delete-all']}
          onClick={() => {
            toast.success('Xóa lịch sử tìm kiếm thành công!');
            clearLocalStorage('history-search');
            setListKeyword([]);
          }}
        >
          Xóa tất cả
        </div>
      </div>
      <div className={styles['body']}>
        {listKeyword?.map((it, id) => {
          return (
            <KeywordSearch
              text={it}
              key={id}
              removeItem={() => {
                setListKeyword((prev) => {
                  let t = [...prev];
                  t.splice (id, 1);
                  setLocalStorage ('history-search', t);
                  // let index = prev.indexOf(it);
                  // let t = prev.slice(0, index);
                  // t = t.concat(prev.slice(index + 1));
                  // setLocalStorage('history-search', t);
                  return t;
                });
              }}
            />
          );
        })}
      </div>
    </div>
  );
}
