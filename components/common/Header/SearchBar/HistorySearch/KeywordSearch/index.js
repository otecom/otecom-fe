import Link from 'next/link';
import { toast } from 'react-toastify';
import styles from './styles.module.scss';

export default function KeywordSearch({ text, removeItem }) {
    return <div className={styles['keyword']}>
        <Link href={{
            pathname: '/',
            query: {
                s: text,
            }
        }}>
            <a className={styles['link']}>{text}

            </a>
        </Link>
        <p className={styles['remove']} onClick={(e) => {
            e.stopPropagation();
            removeItem();
            // toast("Xóa thành công!")
        }}>x</p>
    </div>
}