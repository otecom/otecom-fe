import Link from "next/link";
import FontIcon from "~/components/base/FontIcon";

import styles from './styles.module.scss';

export default function IconNav ({children, label, to = '#'}) {
    return <Link href={to}><a className={styles['icon-nav']}><FontIcon size="26px">{children}</FontIcon><p>{label}</p></a></Link>
}