import Container from '~/components/base/Container';
import LogoHeader from './LogoHeader';
import FontIcon from '~/components/base/FontIcon';
import styles from './styles.module.scss';
import SearchBar from './SearchBar';

import { useGetCategories } from '~/hooks/api/useCategories';
import CategoryOpen from '../CategoryOpen';
import IconNav from './IconNav';
import useResize from '~/hooks/useResize';
import ListNav from './ListNav';
import PopupModal from '~/components/base/PopupModal';
import { useState } from 'react';
import PopupNav from './PopupNav';

export default function Header() {
  const { data: categories } = useGetCategories();
  const [isOpen, setIsOpen] = useState(false);
  const device = useResize();
  return (
    <div className={styles['header']}>
      <Container className={styles['header-container']}>
        <LogoHeader />
        <div className={styles['header-center']}>
          <CategoryOpen data={categories?.data} />
          <SearchBar />
        </div>
        <div className={styles['header-right']}>
          {device === 'pc' ? (
            <>
              <ListNav />
            </>
          ) : (
            <FontIcon
              onClick={() => {
                setIsOpen(!isOpen);
                console.log(isOpen);
              }}
            >
              &#xe5d2;
            </FontIcon>
          )}
        </div>
      </Container>
      <PopupNav isOpen={isOpen} />
    </div>
  );
}
