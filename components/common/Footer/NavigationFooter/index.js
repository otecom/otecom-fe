import NavigateLink from './NavigateLink';
import styles from './styles.module.scss';

export default function NavigationFooter({ className, title = 'Cột 1', listItem = [] }) {
  return (
    <div className={`${styles['navigation-footer']} ${className}`}>
      <h4 className={styles['title']}>{title}</h4>
      <div className={styles['navigation-footer__list']}>
        {listItem?.map((item, index) => {
          return <NavigateLink to={item.slug} text={item.name} key={index} />;
        })}
      </div>
    </div>
  );
}
