import NavigationFooter from './NavigationFooter';
import styles from './styles.module.scss';
import Container from '../../base/Container';
// import Space from '~/components/base/Space';

import baseCss from '~/public/styles/base.module.scss';
import Link from 'next/link';
import { useGetCategories } from '~/hooks/api/useCategories';
import SocialIcon from './SocialIcon';

export default function Footer() {
  const { data: categories } = useGetCategories({
    limit: 4,
  });

  return (
    <footer className={styles['footer']}>
      <Container className={styles['main']}>
        <div className={styles['about-web']}>
          <Link href="/">
            <a>
              <img src="/logo/logo.svg" style={{ height: '100px' }} />
            </a>
          </Link>
          <p className={styles['des']}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis,
            auctor consequat urna. Morbi a bibendum metus. Donec scelerisque sollicitudin enim eu venenatis. Duis
            tincidunt laoreet ex, in pretium orci vestibulum eget.
          </p>
          <div className={styles['social']}>
            <SocialIcon icon={'facebook'} to={'#'} />
            <SocialIcon icon={'youtube'} to={'#'} />
            <SocialIcon icon={'github'} to={'#'} />
            <SocialIcon icon={'twitter'} to={'#'} />
          </div>
        </div>
        <div className={styles['navigate-col']}>
          <NavigationFooter title={'Danh mục'} listItem={categories?.data} />

          <NavigationFooter
            title={'Liên hệ'}
            listItem={[
              {
                name: 'otecom@gmail.com',
                slug: 'mailto:otecom@gmail.com',
              },
              {
                name: '0989113221',
                slug: 'tel:0989113221',
              },
            ]}
          />

          <NavigationFooter
            title={'Dịch vụ'}
            listItem={[
              {
                name: 'Giới thiệu Otecom ',
                slug: '#',
              },
              {
                name: 'Điều khoản sử dụng',
                slug: '#',
              },
            ]}
          />
        </div>
      </Container>
      <div className={styles['hr']}></div>
      <Container>
        <p
          style={{
            textAlign: 'center',
            fontSize: '1.4rem',
          }}
        >
          © {new Date().getFullYear()} Copyright by Otecom
        </p>
      </Container>
    </footer>
  );
}
