import Link from 'next/link';

export default function SocialIcon({ icon, to = '#' }) {
  //https://www.figma.com/file/xmF8ragjOenLhxq1oDYyTW/Social-Media-Icons-2022-(Community)?node-id=802%3A2&t=tmOSdYL6AJXMy85v-0
  return (
    <Link href={to}>
      <a>
        <img src={`/imgs/icons/social/full-color/${icon}.svg`} />
      </a>
    </Link>
  );
}
