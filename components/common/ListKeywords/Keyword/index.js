import styles from './styles.module.scss';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

export default function RainbowItem({ title = '', active = false, path = null, style, onClick = function () {} }) {
  const router = useRouter();
  let activeItem = router?.asPath === path || active;
  useEffect(() => {
    activeItem = false;
  }, [router.query]);
  return (
    <div className={`${styles['keyword-item']} ${activeItem && styles['active']} `} onClick={onClick}>
      {!!path ? (
        <Link href={`${path}`}>
          <a className={styles['link']}></a>
        </Link>
      ) : (
        <></>
      )}
      <p style={style}>{title}</p>
    </div>
  );
}
