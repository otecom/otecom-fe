import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useState } from 'react';
import Keyword from './Keyword';
import styles from './styles.module.scss';

export default function ListKeywords({ data, style, onChange = function () {}, firstActive = true }) {
  const [active, setActive] = useState(firstActive ? 0 : -1);
  const router = useRouter();
  useEffect(() => {
    setActive(firstActive ? 0 : -1);
  }, [router.query]);
  return (
    <div className={styles['keyword-list']}>
      {data?.map((item, index) => {
        return (
          <Keyword
            key={index}
            style={style}
            title={item.title}
            path={item.path}
            active={index === active}
            onClick={() => {
              onChange(item);
              setActive(index);
            }}
          />
        );
      })}
    </div>
  );
}
