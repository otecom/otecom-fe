import Link from 'next/link';
import FontIcon from '~/components/base/FontIcon';
import baseCss from '~/public/styles/base.module.scss';

import styles from './styles.module.scss';

export default function CategoryOpen({ data }) {
  return (
    <div className={styles['category-open']}>
      <FontIcon
        color={baseCss.colorPrimary}
        size="px"
        icon={9810}
        options={{
          fill: 1,
        }}
      >
        &#xE1A0;
      </FontIcon>
      <div className={styles['category-popup']}>
        <h3 className={styles['title']}>Danh mục sản phẩm</h3>
        <div className={styles['list-item']}>
          {data?.map((it, id) => {
            return (
              <Link href={`/category/${it?.slug}`}>
                <a key={id} className={styles['category-item']}>
                  {it?.name}
                </a>
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
}
