import { createContext } from 'react';

// import PopupModal from '~/components/base/PopupModal';
// import PopupRequestAuth from '~/components/common/PopupRequestAuth';

import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import BackdropModal from '~/components/base/BackdropModal';

const GlobalContext = createContext();

const GlobalProvider = ({ children }) => {
  const [isShowBackdrop, setIsShowBackdrop] = useState(false);
  const router = useRouter();
  useEffect(() => {
    setIsShowBackdrop(false);
  }, [router.query]);
  return (
    <>
      {
        isShowBackdrop && <BackdropModal />
      }
      <GlobalContext.Provider
        value={{
          backdropModal: {
            isShowBackdrop,
            setIsShowBackdrop
          }
        }}
      >
        {children}
      </GlobalContext.Provider>
    </>
  );
};

export { GlobalContext };
export default GlobalProvider;
