import { useInfiniteQuery, useQuery } from 'react-query';
import { API } from '~/core/api/config';
import api, { encodeQueryData } from '~/core/api/api';

export async function getProducts(filters, { pageParam = 1 }) {
  filters.page = pageParam;
  filters = encodeQueryData(filters);
  const { data } = await api.get(API.PRODUCT.PRODUCTS + '?' + filters);
  return data;
}

export const useGetProductsInfinite = (filters) => {
  return useInfiniteQuery(['get-products-infinite', filters], (queryKey, pageParam) => getProducts(filters, queryKey), {
    getNextPageParam: (_lastPage, pages) => {
      let data = _lastPage;
      if (data.currentPage < data.lastPage) return pages.length + 1;
      else return undefined;
    },
  });
};

export const useGetProducts = (filters) => {
  return useQuery(['get-products', filters], () => getProducts(filters));
};

export async function getProductDetail({ pageParam = '' }, slug) {
  // const userId = getCookie('auth_user_id');
  let url = API.PRODUCT.PRODUCT_DETAIL;
  // console.log(pageParam, slug);
  if (pageParam) url = url.replace(':slug', pageParam);
  else url = url.replace(':slug', slug);
  // url += '?lang=' + getCookie(CURRENT_LANG_KEY) || 'en';
  // if (userId) url += '&user_id=' + userId;
  const { data } = await api.get(url);
  return data;
}

export const useGetProductDetail = (slug) => {
  return useQuery(['get-product-detail', slug], () => getProductDetail({ pageParam: '' }, slug));
};

export const useGetProductDetailInfinity = (slug) => {
  return useInfiniteQuery(
    ['get-post-detail-infinity', slug],
    (queryKey, pageParam) => getProductDetail(queryKey, slug),
    {
      getNextPageParam: (_lastPage, pages) => {
        return _lastPage?.data?.next_slug ? _lastPage?.data?.next_slug : undefined;
      },
    }
  );
};
