import {  useQuery } from 'react-query';
import { API } from '~/core/api/config';
import api, { encodeQueryData} from "~/core/api/api";

async function getCategories(filters) {
  filters = encodeQueryData(filters);
  const { data } = await api.get(API.CATEGORY.GET_CATEGORIES + "?" + filters);
  return data;
}

export const useGetCategories = (filters) => {
  return useQuery(["get-categories", filters], () => getCategories(filters))
};
