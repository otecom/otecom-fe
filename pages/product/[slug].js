import MainLayout from '~/layout/MainLayout';
import { useRouter } from 'next/router';

import { API } from '~/core/api/config';

// import { QueryClient } from 'react-query';

import PropTypes from 'prop-types';
// import { dehydrate } from 'react-query/hydration';
import { useState } from 'react';
import { useEffect } from 'react';
import { useGetProductDetail } from '~/hooks/api/useProduct';
import ProductContainer from '~/containers/ProductContainer';

export default function ProductDetail({ productDetail }) {
  const router = useRouter();
  const { slug } = router.query;

  const { data: product } = useGetProductDetail(slug);

  const [detail, setDetail] = useState({});

  useEffect(() => {
    setDetail(product?.data);
  }, [product]);

  return (
    <MainLayout meta_data={productDetail} data={detail} title={detail?.title}>
      <ProductContainer data={product?.data} />
    </MainLayout>
  );
}
export async function getServerSideProps(ctx) {
  const {
    params: { slug },
  } = ctx;

  let url = API.PRODUCT.PRODUCT_DETAIL;
  url = url.replace(':slug', slug);
  let data;
  await fetch(process.env.SERVER_URI + url)
    .then(function (response) {
      return response.json();
    })
    .then(function (payload) {
      data = payload;
    })
    .catch((err) => {
      console.log(err);
    });
  return {
    props: {
      productDetail: data?.data || {},
    },
  };
}

ProductDetail.propTypes = {
  productDetail: PropTypes.objectOf(PropTypes.any),
};
