export function setLocalStorage(key, value) {
  if (typeof window != 'undefined') {
    localStorage.setItem(key, value);
  }
}

export function getLocalStorage(key, elseValue) {
  if (typeof window != 'undefined') {
    let value = localStorage.getItem(key);
    return value !== null ? value : elseValue;
  }
  return elseValue;
}

export function removeLocalStorage(key) {
  if (typeof window != 'undefined') {
    localStorage.removeItem(key);
  }
}

export function clearLocalStorage() {
  if (typeof window != 'undefined') {
    localStorage.clear();
  }
}
