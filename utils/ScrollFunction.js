export function lastScrollX(e, scrollBy = 0) {
  let widthScroll = e.scrollMaxX || e.scrollWidth - e.clientWidth;
  if (e.scrollLeft + scrollBy >= widthScroll) {
    return true;
  }
  return false;
}

export function rightScrollView(listItem, item, index, inline = 'start') {
  if (item.length > 0 && index < item.length - 1 && !lastScrollX(listItem)) {
    index += 1;
    item[index].scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
      inline: inline,
    });
  }
  return index;
}

export function leftScrollView(listItem, item, index, inline = 'start') {
  if (item.length > 0 && index > 0) {
    index -= 1;
    item[index].scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
      inline: inline,
    });
  }
  return index;
}
