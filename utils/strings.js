export function hasWhiteSpace(s) {
  return /\s/g.test(s);
}
export function handlePrice(number) {
  return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
}
