import Head from 'next/head';

import Footer from '~/components/common/Footer';
import Header from '~/components/common/Header';

import styles from './styles.module.scss';
import icon from '~/public/favicon.ico';
import ScrollToTop from '~/components/common/ScrollToTop';

export default function MainLayout({ children, title = '', currentPage = '', data = null, meta_data = null }) {
  const titlePage = meta_data ? meta_data?.name : !!title ? 'Otecom - ' + title : 'Otecom';
  return (
    <>
      <script async src="https://www.googletagmanager.com/gtag/js?id=G-BDHNFW2Q5M" />
      <script
        dangerouslySetInnerHTML={{
          __html:
            "window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}gtag('js', new Date()); gtag('config', 'G-BDHNFW2Q5M');",
        }}
      ></script>
      <Head>
        <title>{titlePage}</title>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta
          property="og:title"
          content={
            !!meta_data
              ? meta_data?.seo_title
                ? `Otecom - ${meta_data?.seo_title}`
                : `Otecom - ${meta_data?.name}`
              : title === ''
              ? 'Otecom'
              : `Otecom - ${title}`
          }
        />
        <meta
          property="og:description"
          content={
            !!meta_data
              ? meta_data?.description_seo
                ? `${meta_data?.description_seo}`
                : `${meta_data?.description}`
              : title === ''
              ? 'Otecom'
              : `Otecom - ${title}`
          }
        />
        <meta property="og:url" content={typeof window != 'undefined' ? window.location.href : ''} />
        <meta property="og:image" content={meta_data?.images ? meta_data?.images[0] : icon.src} />
        <meta
          property="og:tag"
          content={`${meta_data?.tag?.map((item, index) => {
            return item;
          })}`}
        />
        <meta name="og:type" content={!!meta_data ? 'article' : 'website'} />

        <link href="/fonts/fonts.css" rel="stylesheet" type="text/css" />
      </Head>

      <div className={`${styles['body']}`}>
        <div className={styles['pseudo']}></div>
        <Header currentPage={currentPage} />
        <div className={`${styles['section']}`}>{children}</div>
        <Footer />
        <ScrollToTop />
      </div>
    </>
  );
}
