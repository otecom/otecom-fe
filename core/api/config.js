export const API_ROOT = process.env.SERVER_URI;

export const TIMEOUT = 15000;

export const API = {
  SLIDER: {
    BANNER: '/slider',
  },
  CATEGORY: {
    GET_CATEGORIES: '/categories',
  },
  PRODUCT: {
    PRODUCTS: '/products',
    PRODUCT_DETAIL: '/product/:slug',
    SEARCH: '/products/search',
  },
};
